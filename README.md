

R 学习交流群：**189059061**

Python 学习交流群：**327473248**

Linux & Shell 学习交流群：**684676195**



<figure class="third"'>
    <a href="#" title="R 学习交流群">
    	<img src="R.png" width="32%"/>
    </a>
    <a href="#" title="Python 学习交流群">
    	<img src="Python.png" width="32%"/>
    </a>
    <a href="#" title="Linux & Shell 学习交流群">
    	<img src="Linux & Shell.png" width="32%"/>
    </a>
</figure>






# A weekly social data project in R

A weekly data project aimed at the R ecosystem. As this project was borne out of the `R4DS Online Learning Community` and the `R for Data Science` textbook, an emphasis was placed on understanding how to summarize and arrange data to make meaningful charts with `ggplot2`, `tidyr`, `dplyr`, and other tools in the `tidyverse` ecosystem. However, any code-based methodology is welcome - just please remember to share the code used to generate the results.

<img src="tt_logo.png">

**官网地址**：Thomas Mock (2022). Tidy Tuesday: A weekly data project aimed at the R ecosystem. https://github.com/rfordatascience/tidytuesday.



# 2018 合集

[2018 合集地址](2018)，点击进入该地址查看所有示例的图片，点击图片可以访问该图片的生成方法。

如果传送出现 **404** 错误，应该就是还没整理到这里；或者 CSDN 传送提示: *啊哦~你想找的内容离你而去了哦*，说明示例已经画完，但还未整理到博客中，尽情期待！

| Week | Date | Data |
| :---: | :---: | :--- |
| 1 | `2018-04-02` | [US Tuition Costs](2018/2018-04-02) |
| 2 | `2018-04-09` | [NFL Positional Salaries](2018/2018-04-09) |
| 3|  `2018-04-16`| [Global Mortality](2018/2018-04-16) |
| 4 | `2018-04-23`| [Australian Salaries by Gender](2018/2018-04-23) |
| 5 | `2018-04-30` | [ACS Census Data (2015)](2018/2018-04-30) |
| 6 | `2018-05-07`| [Global Coffee Chains](2018/2018-05-07) |
| 7 | `2018-05-14` | [Star Wars Survey](2018/2018-05-14) |
| 8 | `2018-05-21` | [US Honey Production](2018/2018-05-21) |
| 9 | `2018-05-29` | [Comic book characters](2018/2018-05-29) |
| 10 | `2018-06-05` | [Biketown Bikeshare](2018/2018-06-05) |
| 11 | `2018-06-12` | [FIFA World Cup Audience](2018/2018-06-12) |
| 12 | `2018-06-19` | [Hurricanes & Puerto Rico](2018/2018-06-19) |
| 13 | `2018-06-26` | [Alcohol Consumption](2018/2018-06-26) |
| 14 | `2018-07-03` | [Global Life Expectancy](2018/2018-07-03) |
| 15 | `2018-07-10` | [Craft Beer USA](2018/2018-07-10) |
| 16 | `2018-07-17` | [Exercise USA](2018/2018-07-17) |
| 17 | `2018-07-23` | [p-hack-athon collaboration](2018/2018-07-23)|
| 18 | `2018-07-31` | [Dallas Animal Shelter FY2017](2018/2018-07-31) |
| 19 | `2018-08-07` | [Airline Safety](2018/2018-08-07) |
| 20 | `2018-08-14` | [Russian Troll Tweets](2018/2018-08-14)|
| 21 | `2018-08-21` | [California Fires](2018/2018-08-21) |
| 22 | `2018-08-28` | [NFL Stats](2018/2018-08-28) |
| 23 | `2018-09-04` | [Fast Food Calories](2018/2018-09-04) |
| 24 | `2018-09-11` | [Cats vs Dogs (USA)](2018/2018-09-11) |
| 25 | `2018-09-18` | [US Flights or Hypoxia](2018/2018-09-18) |
| 26 | `2018-09-25` | [Global Invasive Species](2018/2018-09-25) |
| 27 | `2018-10-02` | [US Births](2018/2018-10-02) |
| 28 | `2018-10-09` | [US Voter Turnout](2018/2018-10-09) |
| 29 | `2018-10-16` | [College Major & Income](2018/2018-10-16) |
| 30 | `2018-10-23` | [Horror Movie Profit](2018/2018-10-23) |
| 31 | `2018-10-30` | [R and R package downloads](2018/2018-10-30) |
| 32 | `2018-11-06` | [US Wind Farm locations](2018/2018-11-06) |
| 33 | `2018-11-13` | [Malaria Data](2018/2018-11-13) |
| 34 | `2018-11-20` | [Thanksgiving Dinner or Transgender Day of Remembrance](2018/2018-11-20) |
| 35 | `2018-11-27` | [Baltimore Bridges](2018/2018-11-27) |
| 36 | `2018-12-04` | [Medium Article Metadata](2018/2018-12-04) |
| 37 | `2018-12-11` | [NYC Restaurant inspections](2018/2018-12-11) |
| 38 | `2018-12-18` | [Cetaceans Data](2018/2018-12-18) |
